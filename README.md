# Virtual Soc
A client/server application written in C that simulates a social network.

As a user I am able to:
 * register/login
 * add friends
 * make posts
 * chat mechanism
 * customize profile
 * set privacy for posts and profile

## Prerequisites
* [CMake](https://cmake.org/)
* [SQLite3](https://sqlite.org/)
* [Qt4](https://www.qt.io/download/)

## Run this project

1 . Clone this project on your machine

	git clone https://gitlab.com/mihaesandrei/virtual-soc.git
2 . Change directory

	cd virtual-soc
    
3 . Build executables

    cmake .
    make

4 . Run the application

    cd bin
    ./createDB
    ./server
    ./client
    