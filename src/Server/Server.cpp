#include "Server.h"

Server::Server(int port) {
    this->port = (uint16_t) port;

    serverDescriptor = createSocket(AF_INET, SOCK_STREAM, 0);
    bindSocket(serverDescriptor, server);
    acceptClients(serverDescriptor);
}

void Server::start() {
    socklen_t len = sizeof(struct sockaddr_in);
    struct sockaddr_in clientAddress;
    while (1) {
        int clientDescriptor = accept(serverDescriptor, (struct sockaddr *) &clientAddress, &len);
        if (clientDescriptor == -1) throw "[server] Error: accept().\n";
        else {
            pthread_t thread;

            User user;
            user.setFileDescriptor(clientDescriptor);
            user.setAddress(clientAddress);

            if (pthread_create(&thread, NULL, MenuO::generate, &user) != 0)
                throw "[server] Error: failed to create thread.\n";
        }
    }
}

int Server::createSocket(int domain, int type, int protocol) {
    int  optval = 1; 			/* optiune folosita pentru setsockopt()*/
    /* creare socket */
    int serverDescriptor;
    if ((serverDescriptor = socket(domain, type, protocol)) == -1) throw "[server] Error: socket().\n";

    /*setam pentru socket optiunea SO_REUSEADDR */
    setsockopt(serverDescriptor, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
    return serverDescriptor;
}

void Server::bindSocket(int &serverDescriptor, sockaddr_in& server) {
    /* pregatim structurile de date */
    bzero(&server, sizeof(server));

    /* umplem structura folosita de server */
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(port);

    /* atasam socketul */
    if (bind(serverDescriptor, (struct sockaddr *) &server, sizeof(struct sockaddr)) == -1)
        throw "[server] Error: bind().\n";
}
void Server::acceptClients(int &serverDescriptor) {
    /* punem serverul sa asculte daca vin clienti sa se conecteze */
    if (listen(serverDescriptor, 0) == -1) throw "[server] Error: listen().\n";

    printf("[server] Port: %d...\n", port);
    fflush(stdout);
}