#ifndef VIRTUALSOC_MENUOPTION_H
#define VIRTUALSOC_MENUOPTION_H

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>
#include <pthread.h>

#include "Database/Database.h"
#include "Database/User.h"

#define MAX_USERS 30

class MenuO {

private:
    static Database *database;
    static User users[];

public:
    static void* generate(void* pnewsock);

private:
    void static signUp(User &user);
    void static logIn(User &user);
    void static logOut(User &user);
    void static quit(User &user);
    void static addFriend(User &user);
    void static viewFriendList(User &user);
    void static privacySettings(User &user);
    void static setProfilePrivacy(User &user);
    void static setPostsPrivacy(User &user);
    void static makePost(User &user);
    void static viewPublicPosts(User &user);
    void static viewRecentPosts(User &user);
    void static createConversation(User &user);
    void static addParticipant(User &user);
    void static addOwnerInParticipants(User &user);
    void static sendMessage(User &user);
    void static getMessages(User &user);
    void static getParticipants(User &user);
    void static signUpAsAdmin(User &user);
    void static logInAsAdmin(User &user);
    void static getUsersList(User &user);
    void static getConversations(User &user);
};

#endif //VIRTUALSOC_MENUOPTION_H
