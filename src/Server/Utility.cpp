#include "Utility.h"

char *Utility::concatenate(char *first, char *second) {
    size_t len1 = strlen(first);
    size_t len2 = strlen(second);

    char *result = (char *) malloc(len1 + len2 + 1);

    memcpy(result, first, len1);
    memcpy(result + len1, second, len2);
    result[len1 + len2] = '\0';
    return result;
}

void Utility::copy(char **first, char *second) {
    const size_t len = strlen(second);
    *first = (char *) malloc(len);
    strcpy(*first, second);
}

char *Utility::conv_addr(sockaddr_in address) {
    char *str = NULL;
    str = (char *) malloc(35);
    char port[7];
    /* adresa IP a clientului */
    strcpy(str, inet_ntoa(address.sin_addr));
    /* portul utilizat de clientDescriptor */
    bzero(port, 7);
    sprintf(port, ":%d", ntohs(address.sin_port));
    strcat(str, port);
    return str;
}