#include "Database.h"

bool Database::count = false;
int Database::adminFound;
User Database::user;
char Database::messages[2000];
vector<string> Database::participants;
int Database::validMail;

Database::Database() {
    data = "Callback function called";
    /* Open database */
    rc = sqlite3_open("database.db", &db);

    if (rc) {
        fprintf(stderr, "[server] Can't open database: %s\n", sqlite3_errmsg(db));
        exit(0);
    }
    else fprintf(stderr, "[server] Opened database successfully\n");
}

Database::~Database() {
    sqlite3_close(db);
}

bool Database::findUser(User &user) {
    /* Create SQL statement
     *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * SELECT COUNT(*) FROM users WHERE password = user.getPassword()  *
     *                              AND    email = user.getEmail();    *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     *
     */
    sql = (char *) "SELECT COUNT(*) from USERS WHERE PASSWORD=\'";
    sql = Utility::concatenate(sql, user.getPassword());
    sql = Utility::concatenate(sql, (char *) "\' AND email=\'");
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "\';");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_find, (void *) data, &zErrMsg);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }

    if (count)
        fprintf(stdout, "[server] Client %d logged in succesfully with E-mail \"%s\".\n", user.getFileDescriptor(),
                user.getEmail());
    else fprintf(stdout, "[server] Client %d has introduced invalid email/password.\n", user.getFileDescriptor());

    return count;
}

int Database::callback_find(void *data, int argc, char **argv, char **azColName) {
    for (int i = 0; i < argc; i++) count = (bool) atoi(argv[i]); /* count will store number of lines*/
    return 0;
}

bool Database::insertUser(User &user) {
    /* Create SQL statement
     *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * INSERT INTO USERS (firstname, surname, password, email, profile_privacy, posts_privacy, status)   *
     *            VALUES (user.getFirstName(), user.getSurname(), user.getPassword(), user.getEmail(),   *
     *                    'PUBLIC', 'PUBLIC', OFFLINE');                                                 *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     */
    sql = (char *) "INSERT INTO USERS (FIRSTNAME, SURNAME, PASSWORD, EMAIL, PROFILE_PRIVACY, POSTS_PRIVACY, STATUS) "
                    "VALUES ('";
    sql = Utility::concatenate(sql, user.getFirstName());
    sql = Utility::concatenate(sql, (char *) "', \'");
    sql = Utility::concatenate(sql, user.getSurname());
    sql = Utility::concatenate(sql, (char *) "', \'");
    sql = Utility::concatenate(sql, user.getPassword());
    sql = Utility::concatenate(sql, (char *) "', \'");
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "\', 'Public', 'Public', 'Offline');");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_insert, 0, &zErrMsg);
    bool inserted;
    if (rc != SQLITE_OK) {
        inserted = false;
        fprintf(stdout, "[server] Client is trying to register, but E-mail \"%s\" is already in database!\n",  user.getEmail());
    } else {
        inserted = true;
        fprintf(stdout, "[server] User [%s, %s, %s, %s] have been registered successfully.\n", user.getFirstName(),
                user.getSurname(), user.getPassword(), user.getEmail());
    }
    return inserted;
}

void Database::setStatusOnline(User &user) {
    /* Create merged SQL statement
     *
     * * * * * * * * * * * * * * * * * * * * * * * * *
     * UPDATE USERS SET  status = 'ONLINE',          *
     *                       ip = user.getAddress()  *
     *              WHERE email = user.getEmail() ;  *
     * * * * * * * * * * * * * * * * * * * * * * * * *
     *
     */
    sql = (char *) "UPDATE USERS set STATUS = 'Online', IP ='";
    sql = Utility::concatenate(sql, user.getAddress());
    sql = Utility::concatenate(sql, (char *) "' where EMAIL='");
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "';");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_updateStatus, (void *) data, &zErrMsg);
    if (rc != SQLITE_OK) {  fprintf(stderr, "SQL error: %s\n", zErrMsg); sqlite3_free(zErrMsg); }
}

void Database::setStatusOffline(User &user) {
    /* Create merged SQL statement
     *
     * * * * * * * * * * * * * * * * * * * * * * * * *
     * UPDATE USERS SET status = 'Offline',          *
     *                      ip = NULL                *
     *             WHERE email = user.getEmail() ;   *
     * * * * * * * * * * * * * * * * * * * * * * * * *
     */
    sql = (char *) "UPDATE USERS set STATUS = 'Offline', IP = NULL where EMAIL='";
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "';");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_updateStatus, (void *) data, &zErrMsg);
    if (rc != SQLITE_OK) { fprintf(stderr, "SQL error: %s\n", zErrMsg); sqlite3_free(zErrMsg); }
    else fprintf(stdout, "[server] Client %d logged out succesfully.\n", user.getFileDescriptor());
}

int Database::callback_updateStatus(void *data, int argc, char **argv, char **azColName) {
    int i;
    for (i = 0; i < argc; i++)
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    return 0;
}

bool Database::insertFriendship(User &user1, User& user2, char *type) {
    /* Create SQL statement */
    sql = (char *) "INSERT INTO friendship (FIRST_ID, SECOND_ID, TYPE) VALUES( ";
    sql = Utility::concatenate(sql, (char *) "(SELECT id FROM users WHERE email='");
    sql = Utility::concatenate(sql, user1.getEmail());
    sql = Utility::concatenate(sql, (char *) "'), ");
    sql = Utility::concatenate(sql, (char *) "(SELECT id FROM users WHERE email='");
    sql = Utility::concatenate(sql, user2.getEmail());
    sql = Utility::concatenate(sql, (char *) "'), '");
    sql = Utility::concatenate(sql, type);
    sql = Utility::concatenate(sql, (char *)"');");
    sql = Utility::concatenate(sql, (char *) "INSERT INTO friendship (FIRST_ID, SECOND_ID, TYPE) VALUES( ");
    sql = Utility::concatenate(sql, (char *) "(SELECT id FROM users WHERE email='");
    sql = Utility::concatenate(sql, user2.getEmail());
    sql = Utility::concatenate(sql, (char *) "'), ");
    sql = Utility::concatenate(sql, (char *) "(SELECT id FROM users WHERE email='");
    sql = Utility::concatenate(sql, user1.getEmail());
    sql = Utility::concatenate(sql, (char *) "'), '");
    sql = Utility::concatenate(sql, type);
    sql = Utility::concatenate(sql, (char *)"');");

    /* Execute SQL statement */
    bool inserted;
    rc = sqlite3_exec(db, sql, callback_insert, 0, &zErrMsg);
    if (rc != SQLITE_OK) {
        inserted = false;
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        fprintf(stdout, "[server] Error: insertFriendship()\n");
    } else {
        inserted = true;
        fprintf(stdout, "[server] Friendship inserted between %s and %s\n.", user1.getEmail(), user2.getEmail());
    }
    return inserted;
}
int Database::callback_insert(void *NotUsed, int argc, char **argv, char **azColName) {
    int i;
    for (i = 0; i < argc; i++)
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    return 0;
}

void Database::getFriendList(User &user) {

    this->user = user;
    /* Create SQL statement */
    sql = (char *) "SELECT u.email, f.type, u.status FROM users u JOIN friendship f " \
                   "ON u.id = f.first_id " \
                   "WHERE f.second_id = (SELECT id FROM users " \
                                        "WHERE email = \'";
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char* ) "\');");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_getFriendList, (void*)data, &zErrMsg);
    if( rc != SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        printf("[server] Client %d couldn't receive his friend list.\n", user.getFileDescriptor());
    }
    else printf("[server] Client %d received his friend list.\n", user.getFileDescriptor());
}

int Database::callback_getFriendList(void *data, int argc, char **argv, char **azColName) {
    char *row;
    row = Utility::concatenate(argv[0], (char *) " - ");
    row = Utility::concatenate(row, argv[1]);
    row = Utility::concatenate(row, (char *) " - ");
    row = Utility::concatenate(row, argv[2]);

    bool received;
    if (read(user.getFileDescriptor(), &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(user.getFileDescriptor(), row, strlen(row)) < 0) throw "[server] Error: write() to client.\n";
    return 0;
}

bool Database::getUserPrivacy(User &user) {

    this->user = user;
    /* Create SQL statement */
    sql = (char *) "SELECT PROFILE_PRIVACY, POSTS_PRIVACY from USERS WHERE EMAIL=\'";
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "';");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_getUserPrivacy, (void *) data, &zErrMsg);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return false;
    }
    else {return true;}
}

int Database::callback_getUserPrivacy(void *data, int argc, char **argv, char **azColName) {

    /* Profile Privacy = argv[0], Posts Privacy = argv[1] */
    if (write(user.getFileDescriptor(), argv[0], strlen(argv[0])) < 0)
        throw "[server] Error: write() to client.\n";

    bool ready;
    if (read(user.getFileDescriptor(), &ready, sizeof(ready)) < 0)
        throw "[client]Eroare la read() de la server.\n";

    if (write(user.getFileDescriptor(), argv[1], strlen(argv[1])) < 0)
        throw "[server] Error: write() to client.\n";
    return 0;
}

void Database::setProfilePrivacy(User &user, char* value) {

    /* Create merged SQL statement */
    sql = (char *) "UPDATE USERS set PROFILE_PRIVACY = '";
    sql = Utility::concatenate(sql, value);
    sql = Utility::concatenate(sql, (char *) "' where EMAIL='");
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "';");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_setPrivacy, (void *) data, &zErrMsg);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
}

void Database::setPostsPrivacy(User &user, char *value) {

    /* Create merged SQL statement */
    sql = (char *) "UPDATE USERS set POSTS_PRIVACY = '";
    sql = Utility::concatenate(sql, value);
    sql = Utility::concatenate(sql, (char *) "' where EMAIL='");
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "';");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_setPrivacy, (void *) data, &zErrMsg);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
}

int Database::callback_setPrivacy(void *data, int argc, char **argv, char **azColName) {
    int i;
    fprintf(stderr, "%s: ", (const char*)data);
    for(i=0; i<argc; i++){
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    return 0;
}

void Database::insertPost(User &user, char *post, char* privacy) {

    /* Create merged SQL statement */
    sql = (char *) "INSERT INTO posts (USER_ID, POST_DATE, POST, POST_PRIVACY) VALUES ((SELECT id FROM users WHERE"
            " email = '";
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "'), datetime(CURRENT_TIMESTAMP, 'localtime'), '");
    sql = Utility::concatenate(sql, post);
    sql = Utility::concatenate(sql, (char *) "', '");
    sql = Utility::concatenate(sql, privacy);
    sql = Utility::concatenate(sql, (char *) "');");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_setPrivacy, (void *) data, &zErrMsg);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
}

void Database::selectPublicPosts(User &user) {

    this->user = user;
    /* Create merged SQL statement */
    sql = (char *) "SELECT p.post_date, u.firstname, u.surname, p.post FROM users u "
                   "JOIN posts p ON u.id = p.user_id "
                   "WHERE p.post_privacy = 'Public' AND u.posts_privacy = 'Public' ";
    sql = Utility::concatenate(sql, (char *) "ORDER BY p.post_date DESC;");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_publicPosts, (void *) data, &zErrMsg);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        printf("[server] Client %d couldn't receive public posts.\n", user.getFileDescriptor());
    }
    else printf("[server] Client %d received public posts.\n", user.getFileDescriptor());
}

int Database::callback_publicPosts(void *data, int argc, char **argv, char **azColName) {

    char buffer[150]; bzero(buffer, 150);
    sprintf(buffer, "Post submited at: %s\n%s %s : %s\n", argv[0], argv[1], argv[2], argv[3]);

    bool received;
    if (read(user.getFileDescriptor(), &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(user.getFileDescriptor(), buffer, strlen(buffer)) < 0) throw "[server] Error: write() to client.\n";

    return 0;
}

void Database::selectRecentPosts(User &user) {

    this->user = user;
    /* Create merged SQL statement */
    sql = (char *) "SELECT p.post_date, u.firstname, u.surname, p.post FROM users u "
                   "JOIN POSTS P on p.user_id = u.id "
                   "WHERE p.user_id != (SELECT id FROM users WHERE email = '";
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "') AND u.posts_privacy = 'Public' "
                                             "AND ( p.post_privacy = (  SELECT type from friendship "
            "WHERE first_id = (SELECT id FROM users WHERE email = '");
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "') AND second_id = p.user_id) OR p.post_privacy = 'Public') "
            "ORDER BY p.post_date DESC;");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_recentPosts, (void *) data, &zErrMsg);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        printf("[server] Client %d couldn't receive recent posts.\n", user.getFileDescriptor());
    }
    else printf("[server] Client %d received recent posts.\n", user.getFileDescriptor());
}

int Database::callback_recentPosts(void *data, int argc, char **argv, char **azColName) {
    char buffer[150]; bzero(buffer, 150);
    sprintf(buffer, "Post submited at: %s\n%s %s : %s\n", argv[0], argv[1], argv[2], argv[3]);

    bool received;
    if (read(user.getFileDescriptor(), &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(user.getFileDescriptor(), buffer, strlen(buffer)) < 0)
        throw "[server] Error: write() to client.\n";
    return 0;;
}

bool Database::insertConversation(User &user, char *conversationName) {
    sql = (char *) "INSERT INTO CONVERSATION (NAME, OWNER_EMAIL) "
            "VALUES ('";
    sql = Utility::concatenate(sql, conversationName);
    sql = Utility::concatenate(sql, (char *) "', \'");
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "'); ");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_insert, 0, &zErrMsg);
    bool inserted;
    if (rc != SQLITE_OK) {
        inserted = false;
        fprintf(stdout, "[server] Client is trying to create a conversation, but name \"%s\" is already in use!\n",
                conversationName);
    } else {
        inserted = true;
        fprintf(stdout, "[server] Conversation [%s, %s] have been inserted successfully.\n", conversationName,
                user.getEmail());
    }
    return inserted;
}

bool Database::insertParticipant(User &user, char *conversationName, char *participantEmail) {
    sql = (char *) "INSERT INTO participants(CONVERSATION_NAME, PARTICIPANT_EMAIL) "
            "VALUES ('";
    sql = Utility::concatenate(sql, conversationName);
    sql = Utility::concatenate(sql, (char *) "', \'");
    sql = Utility::concatenate(sql, participantEmail);
    sql = Utility::concatenate(sql, (char *) "'); ");

    bool inserted;
    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_insert, 0, &zErrMsg);
    if (rc != SQLITE_OK) {
        inserted = false;
        fprintf(stdout,
                "[server] Client is trying to insert participant to conversation, but he is already in there!\n");
    } else {
        inserted = true;
        fprintf(stdout, "[server] Participant [%s, %s] have been inserted successfully.\n", conversationName,
                participantEmail);
    }
    return inserted;
}

bool Database::insertOwnerInParticipants(User &user, char *conversationName) {
    sql = (char *) "INSERT INTO participants(CONVERSATION_NAME, PARTICIPANT_EMAIL) "
            "VALUES ('";
    sql = Utility::concatenate(sql, conversationName);
    sql = Utility::concatenate(sql, (char *) "', \'");
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "'); ");

    bool inserted;
    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_insert, 0, &zErrMsg);
    if (rc != SQLITE_OK) {
        inserted = false;
        fprintf(stdout,
                "[server] Client is trying to insert participant to conversation, but he is already in there!\n");
    } else {
        inserted = true;
        fprintf(stdout, "[server] Participant [%s, %s] have been inserted successfully.\n", conversationName,
                user.getEmail());
    }
    return inserted;
}

void Database::insertMessage(User &user, char *conversationName, char *message) {
    sql = (char *) "INSERT INTO MESSAGES(CONVERSATION_NAME, MESSAGE, SENDER_EMAIL, DATE) "
            "VALUES ('";
    sql = Utility::concatenate(sql, conversationName);
    sql = Utility::concatenate(sql, (char *) "', \'");
    sql = Utility::concatenate(sql, message);
    sql = Utility::concatenate(sql, (char *) "', \'");
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "', datetime(CURRENT_TIMESTAMP, 'localtime')); ");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_insert, 0, &zErrMsg);
    if (rc != SQLITE_OK) {

        fprintf(stdout,
                "[server] Client is trying to send message!\n");
    } else {
        fprintf(stdout, "[server] Message [%s, %s] have been inserted successfully.\n", conversationName,
                message);
    }
}

char* Database::getMessages(User &user, char *conversationName) {
    strcpy(messages, "");
    this->user = user;
    /* Create merged SQL statement */
    sql = (char *) "SELECT sender_email, message FROM messages WHERE conversation_name = '";
    sql = Utility::concatenate(sql, conversationName);
    sql = Utility::concatenate(sql, (char *) "';");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_getMessages, (void *) data, &zErrMsg);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        printf("[server] Client %d couldn't received messages.\n", user.getFileDescriptor());
    }
    else printf("[server] Client %d received messages.\n", user.getFileDescriptor());

    return messages;
}

int Database::callback_getMessages(void *data, int argc, char **argv, char **azColName) {
    char *message = (char *) "";
    message = Utility::concatenate(argv[0], (char *) ": ");
    message = Utility::concatenate(message, argv[1]);
    message = Utility::concatenate(message, (char *) "\n");
    strcat(messages, message);
    return 0;
}

vector<string> Database::getParticipants(User &user, char *conversationName) {

    participants.clear();

    this->user = user;

    /* Create merged SQL statement */
    sql = (char *) "SELECT PARTICIPANT_EMAIL FROM participants WHERE CONVERSATION_NAME = '";
    sql = Utility::concatenate(sql, conversationName);
    sql = Utility::concatenate(sql, (char *) "' AND PARTICIPANT_EMAIL != '");
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "' ;");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_getParticipants, (void *) data, &zErrMsg);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        printf("[server] Client %d couldn't received participants.\n", user.getFileDescriptor());
    }
    else printf("[server] Client %d received participants.\n", user.getFileDescriptor());

    return participants;
}

int Database::callback_getParticipants(void *data, int argc, char **argv, char **azColName) {

    participants.push_back(argv[0]);
    bool received;
    if (read(user.getFileDescriptor(), &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(user.getFileDescriptor(), argv[0], strlen(argv[0])) < 0)
        throw "[server] Error: write() to client.\n";
    return 0;
}

bool Database::insertAdministrator(User &user) {
    sql = (char *) "INSERT INTO Administrators (FIRSTNAME, SURNAME, PASSWORD, EMAIL) "
            "VALUES ('";
    sql = Utility::concatenate(sql, user.getFirstName());
    sql = Utility::concatenate(sql, (char *) "', \'");
    sql = Utility::concatenate(sql, user.getSurname());
    sql = Utility::concatenate(sql, (char *) "', \'");
    sql = Utility::concatenate(sql, user.getPassword());
    sql = Utility::concatenate(sql, (char *) "', \'");
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "');");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_insert, 0, &zErrMsg);
    bool inserted;
    if (rc != SQLITE_OK) {
        inserted = false;
        fprintf(stdout, "[server] Client is trying to register as Administrator, but E-mail \"%s\" is already in database!\n",  user.getEmail());
    } else {
        inserted = true;
        fprintf(stdout, "[server] Admin [%s, %s, %s, %s] have been registered successfully.\n", user.getFirstName(),
                user.getSurname(), user.getPassword(), user.getEmail());
    }
    return inserted;
}

int Database::validEmail(User &user, char* email) {
    sql = (char *) "SELECT COUNT(*) from validEmails WHERE EMAIL=\'";
    sql = Utility::concatenate(sql, email);
    sql = Utility::concatenate(sql, (char *) "';");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_validEmail, (void *) data, &zErrMsg);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }

    return validMail;
}

int Database::callback_validEmail(void *data, int argc, char **argv, char **azColName) {
    validMail = atoi(argv[0]);
    return 0;
}

int Database::findAdministrator(User &user) {
    sql = (char *) "SELECT COUNT(*) from Administrators WHERE PASSWORD=\'";
    sql = Utility::concatenate(sql, user.getPassword());
    sql = Utility::concatenate(sql, (char *) "\' AND email=\'");
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "\';");

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_findAdministrator, (void *) data, &zErrMsg);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    return adminFound;
}

int Database::callback_findAdministrator(void *data, int argc, char **argv, char **azColName) {
    adminFound = atoi(argv[0]);
    return 0;
}

void Database::getUsersList(User &user) {
    this->user = user;
    /* Create SQL statement */
    sql = (char *) "SELECT firstname, surname, email, status, ifnull(ip,'NULL') FROM users;";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_getUsersList, (void*)data, &zErrMsg);
    if( rc != SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        printf("[server] Admin %d couldn't receive users list.\n", user.getFileDescriptor());
    }
    else printf("[server] Admin %d received users list.\n", user.getFileDescriptor());
}

int Database::callback_getUsersList(void *data, int argc, char **argv, char **azColName) {
    char *row;
    row = Utility::concatenate(argv[0], (char *) " - ");
    row = Utility::concatenate(row, argv[1]);
    row = Utility::concatenate(row, (char *) " - ");
    row = Utility::concatenate(row, argv[2]);
    row = Utility::concatenate(row, (char *) " - ");
    row = Utility::concatenate(row, argv[3]);
    row = Utility::concatenate(row, (char *) " - ");
    row = Utility::concatenate(row, argv[4]);

    bool received;
    if (read(user.getFileDescriptor(), &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(user.getFileDescriptor(), row, strlen(row)) < 0) throw "[server] Error: write() to client.\n";
    return 0;
}

void Database::getConversationsList(User &user) {

    this->user = user;

    /* Create merged SQL statement */
    sql = (char *) "select conversation_name from participants where participant_email = '";
    sql = Utility::concatenate(sql, user.getEmail());
    sql = Utility::concatenate(sql, (char *) "' ;");

    printf("%s\n", sql);

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback_getConversationsList, (void *) data, &zErrMsg);
    if (rc != SQLITE_OK) {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        printf("[server] Client %d couldn't received conversations list.\n", user.getFileDescriptor());
    }
    else printf("[server] Client %d received conversations list.\n", user.getFileDescriptor());

}

int Database::callback_getConversationsList(void *data, int argc, char **argv, char **azColName) {
    bool received;
    if (read(user.getFileDescriptor(), &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(user.getFileDescriptor(), argv[0], strlen(argv[0])) < 0)
        throw "[server] Error: write() to client.\n";
    return 0;
}
