#ifndef VIRTUALSOC_USER_H
#define VIRTUALSOC_USER_H

#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>

#include "../Utility.h"

class User {

private:
    char *firstname;
    char *surname;
    char *email;
    char *password;
    int fileDescriptor;
    char *address;


public:
    User() { }
    User(User &user);

    /* getters */
    char *getFirstName();
    char *getSurname();
    char *getEmail();
    char *getPassword();
    int getFileDescriptor();
    char *getAddress();


    /* setters */
    void setFirstName(char *firstname);
    void setSurname(char *surname);
    void setEmail(char *email);
    void setPassword(char *password);
    void setFileDescriptor(int fileDescriptor);
    void setAddress(sockaddr_in address);
    void setAddress(char *address);
};

#endif //VIRTUALSOC_USER_H
