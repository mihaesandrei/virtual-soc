#include "MenuOption.h"

Database *MenuO::database = new Database();
User MenuO::users[MAX_USERS];

void*MenuO::generate(void* pUser) {
    User user;
    user = *(User *) pUser;
    int fd = user.getFileDescriptor();
    users[fd].setFileDescriptor(fd);
    users[fd].setAddress(user.getAddress());

    printf("[server] Client %d has connected from the address %s.\n\n",
           users[fd].getFileDescriptor(), users[fd].getAddress());

    while(1) {
        char menu[100];
        if ( read(fd, menu, (int) sizeof(menu)) < 0) throw "Error: read() from client.\n";
        if (strcmp(menu, "signUp") == 0) { MenuO::signUp(users[fd]); }
        if (strcmp(menu, "logIn") == 0) { MenuO::logIn(users[fd]); }
        if (strcmp(menu, "logOut") == 0) { MenuO::logOut(users[fd]); }
        if (strcmp(menu, "quit") == 0) { MenuO::quit(users[fd]); break;}
        if (strcmp(menu, "addFriend") == 0) { MenuO::addFriend(users[fd]); }
        if (strcmp(menu, "viewFriendList") == 0) { MenuO::viewFriendList(users[fd]); }
        if (strcmp(menu, "privacySettings") == 0) { MenuO::privacySettings(users[fd]); }
        if (strcmp(menu, "setProfilePrivacy") == 0) { MenuO::setProfilePrivacy(users[fd]); }
        if (strcmp(menu, "setPostsPrivacy") == 0) { MenuO::setPostsPrivacy(users[fd]); }
        if (strcmp(menu, "makePost") == 0) { MenuO::makePost(users[fd]); }
        if (strcmp(menu, "viewPublicPosts") == 0) { MenuO::viewPublicPosts(users[fd]); }
        if (strcmp(menu, "viewRecentPosts") == 0) { MenuO::viewRecentPosts(users[fd]); }
        if (strcmp(menu, "createConversation") == 0) { MenuO::createConversation(users[fd]); }
        if (strcmp(menu, "addParticipant") == 0) { MenuO::addParticipant(users[fd]); }
        if (strcmp(menu, "addOwnerInParticipants") == 0) { MenuO::addOwnerInParticipants(users[fd]); }
        if (strcmp(menu, "sendMessage") == 0) { MenuO::sendMessage(users[fd]); }
        if (strcmp(menu, "getMessages") == 0) { MenuO::getMessages(users[fd]); }
        if (strcmp(menu, "getParticipants") == 0) { MenuO::getParticipants(users[fd]); }
        if (strcmp(menu, "signUpAsAdmin") == 0) { MenuO::signUpAsAdmin(users[fd]); }
        if (strcmp(menu, "logInAsAdmin") == 0) { MenuO::logInAsAdmin(users[fd]); }
        if (strcmp(menu, "getUsersList") == 0) { MenuO::getUsersList(users[fd]); }
        if (strcmp(menu, "getConversationsList") == 0) { MenuO::getConversations(users[fd]); }
    }
}

void MenuO::signUp(User &user) {
    char firstname[100], surname[100], password[100], email[100];

    bool readyToRead = true;

    /* Firstname */
    if (write(user.getFileDescriptor(), &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
    if (read(user.getFileDescriptor(), firstname, (int) sizeof(firstname)) < 0) throw "Error: read() from client.\n";

    /* Surname */
    if (write(user.getFileDescriptor(), &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
    if (read(user.getFileDescriptor(), surname, (int) sizeof(surname)) < 0) throw "Error: read() from client.\n";

    /* Password */
    if (write(user.getFileDescriptor(), &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
    if (read(user.getFileDescriptor(), password, (int) sizeof(password)) < 0) throw "Error: read() from client.\n";

    /* Email */
    if (write(user.getFileDescriptor(), &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
    if (read(user.getFileDescriptor(), email, (int) sizeof(email)) < 0) throw "Error: read() from client.\n";

    user.setFirstName(firstname);
    user.setSurname(surname);
    user.setEmail(email);
    user.setPassword(password);

    bool inserted = database->insertUser(user);
    if (write(user.getFileDescriptor(), &inserted, sizeof(inserted)) < 0) throw "[server] Error: write() to client.\n";
}

void MenuO::logIn(User &user) {
    fflush(stdout);
    char email[100], password[100];

    if (read(user.getFileDescriptor(), email, (int) sizeof(email)) < 0) throw "Error: read() from client.\n";
    bool readyToRead = true;
    if (write(user.getFileDescriptor(), &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
    if (read(user.getFileDescriptor(), password, (int) sizeof(password)) < 0)throw "Error: read() from client.\n";

    user.setEmail(email);
    user.setPassword(password);

    bool found = database->findUser(user);
    if (found) { database->setStatusOnline(user); }

    if (write(user.getFileDescriptor(), &found, sizeof(found)) < 0) throw "[server] Error: write() to client.\n";
}

void MenuO::logOut(User &user) {
    database->setStatusOffline(user);
}

void MenuO::quit(User &user) {
    printf("[server] Client %d has disconnected.\n", user.getFileDescriptor());
    close(user.getFileDescriptor());        /* inchidem conexiunea cu clientul */
    pthread_detach(pthread_self());
}

void MenuO::addFriend(User& user1) {
    char friendEmail[100], type[100];

    if (read(user1.getFileDescriptor(), friendEmail, (int) sizeof(friendEmail)) < 0) throw "Error: read() from client.\n";
    User user2;
    user2.setEmail(friendEmail);

    if (read(user1.getFileDescriptor(), type, (int) sizeof(type)) < 0) throw "Error: read() from client.\n";

    bool inserted = database->insertFriendship(user1, user2, type);
    if (write(user1.getFileDescriptor(), &inserted, sizeof(inserted)) < 0) throw "[server] Error: write() to client.\n";
}

void MenuO::viewFriendList(User &user) {
    database->getFriendList(user);
    bool received;
    if (read(user.getFileDescriptor(), &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(user.getFileDescriptor(), "break", 6) < 0)throw "[server] Error: write() to client.\n";
}

void MenuO::privacySettings(User &user) {
   if(database->getUserPrivacy(user))
       printf("[server] Client %d received his privacy settings.\n", user.getFileDescriptor());
}

void MenuO::setProfilePrivacy(User &user) {
    char value[25];
    if (read(user.getFileDescriptor(), value, (int) sizeof(value)) < 0) throw "Error: read() from client.\n";

    database->setProfilePrivacy(user, value);
}

void MenuO::setPostsPrivacy(User &user) {
    char value[25];
    if (read(user.getFileDescriptor(), value, (int) sizeof(value)) < 0) throw "Error: read() from client.\n";

    database->setPostsPrivacy(user, value);
}

void MenuO::makePost(User &user) {
    char post[100], privacy[100];
    bzero(post, 100); bzero(privacy, 100);

    if (read(user.getFileDescriptor(), post, (int) sizeof(post)) < 0) throw "Error: read() from client.\n";

    bool readyToRead = true;
    if (write(user.getFileDescriptor(), &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";

    if (read(user.getFileDescriptor(), privacy, (int) sizeof(privacy)) < 0) throw "Error: read() from client.\n";
    database->insertPost(user, post, privacy);
}

void MenuO::viewPublicPosts(User &user) {
    database->selectPublicPosts(user);

    bool received;
    if (read(user.getFileDescriptor(), &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(user.getFileDescriptor(), "break", 6) < 0) throw "[server] Error: write() to client.\n";
}

void MenuO::viewRecentPosts(User &user) {
    database->selectRecentPosts(user);

    bool received;
    if (read(user.getFileDescriptor(), &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(user.getFileDescriptor(), "break", 6) < 0) throw "[server] Error: write() to client.\n";
}

void MenuO::createConversation(User &user) {


    bool readyToRead = true;
    if (write(user.getFileDescriptor(), &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";

    char conversationName[100]; bzero(conversationName, 100);
    if (read(user.getFileDescriptor(), &conversationName, (int) sizeof(conversationName)) < 0) throw "Error: read() from client.\n";

    bool inserted = database->insertConversation(user, conversationName);
    if (write(user.getFileDescriptor(), &inserted, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";

}

void MenuO::addParticipant(User &user) {

    char conversationName[100]; bzero(conversationName, 100);
    if (read(user.getFileDescriptor(), &conversationName, (int) sizeof(conversationName)) < 0) throw "Error: read() from client.\n";

    char participantEmail[100]; bzero(participantEmail, 100);
    if (read(user.getFileDescriptor(), &participantEmail, (int) sizeof(participantEmail)) < 0) throw "Error: read() from client.\n";


    bool inserted = database->insertParticipant(user, conversationName, participantEmail);
    if (write(user.getFileDescriptor(), &inserted, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";

}

void MenuO::addOwnerInParticipants(User &user) {

    char conversationName[100]; bzero(conversationName, 100);
    if (read(user.getFileDescriptor(), &conversationName, (int) sizeof(conversationName)) < 0) throw "Error: read() from client.\n";

    bool inserted = database->insertOwnerInParticipants(user, conversationName);
    if (write(user.getFileDescriptor(), &inserted, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
}

void MenuO::sendMessage(User &user) {

    char conversationName[100]; bzero(conversationName, 100);
    if (read(user.getFileDescriptor(), &conversationName, (int) sizeof(conversationName)) < 0) throw "Error: read() from client.\n";

    char message[100]; bzero(message, 100);
    if (read(user.getFileDescriptor(), &message, (int) sizeof(message)) < 0) throw "Error: read() from client.\n";

    database->insertMessage(user, conversationName, message);

}

void MenuO::getMessages(User &user) {

    char conversationName[100]; bzero(conversationName, 100);
    if (read(user.getFileDescriptor(), &conversationName, (int) sizeof(conversationName)) < 0) throw "Error: read() from client.\n";

    char messages[2000];
    strcpy(messages, database->getMessages(user, conversationName));
    if (write(user.getFileDescriptor(), &messages, sizeof(messages)) < 0) throw "[client]Error: write() to server.\n";
}

void MenuO::getParticipants(User &user) {

    char conversationName[100]; bzero(conversationName, 100);
    if (read(user.getFileDescriptor(), &conversationName, (int) sizeof(conversationName)) < 0) throw "Error: read() from client.\n";

    vector<string> participants = database->getParticipants(user, conversationName);

    if (write(user.getFileDescriptor(), "break", 6) < 0) throw "[server] Error: write() to client.\n";

}

void MenuO::signUpAsAdmin(User &user) {
    char firstname[100], surname[100], password[100], email[100];

    bool readyToRead = true;

    /* Firstname */
    if (write(user.getFileDescriptor(), &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
    if (read(user.getFileDescriptor(), firstname, (int) sizeof(firstname)) < 0) throw "Error: read() from client.\n";

    /* Surname */
    if (write(user.getFileDescriptor(), &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
    if (read(user.getFileDescriptor(), surname, (int) sizeof(surname)) < 0) throw "Error: read() from client.\n";

    /* Password */
    if (write(user.getFileDescriptor(), &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
    if (read(user.getFileDescriptor(), password, (int) sizeof(password)) < 0) throw "Error: read() from client.\n";

    /* Email */
    if (write(user.getFileDescriptor(), &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
    if (read(user.getFileDescriptor(), email, (int) sizeof(email)) < 0) throw "Error: read() from client.\n";

    user.setFirstName(firstname);
    user.setSurname(surname);
    user.setEmail(email);
    user.setPassword(password);

    int emailFound = database->validEmail(user, email);

    bool inserted;
    if (emailFound)  inserted = database->insertAdministrator(user);
    else inserted = false;
    if (write(user.getFileDescriptor(), &inserted, sizeof(inserted)) < 0)
        throw "[server] Error: write() to client.\n";
}

void MenuO::logInAsAdmin(User &user) {

    fflush(stdout);
    char email[100], password[100];

    if (read(user.getFileDescriptor(), email, (int) sizeof(email)) < 0) throw "Error: read() from client.\n";
    bool readyToRead = true;
    if (write(user.getFileDescriptor(), &readyToRead, sizeof(bool)) < 0) throw "[client]Error: write() to server.\n";
    if (read(user.getFileDescriptor(), password, (int) sizeof(password)) < 0)throw "Error: read() from client.\n";

    user.setEmail(email);
    user.setPassword(password);

    int result = database->findAdministrator(user);
    bool found = result == 1;
    if (write(user.getFileDescriptor(), &found, sizeof(found)) < 0) throw "[server] Error: write() to client.\n";
}

void MenuO::getUsersList(User &user) {
    database->getUsersList(user);
    bool received;
    if (read(user.getFileDescriptor(), &received, (int) sizeof(received)) < 0) throw "Error: read() from client.\n";
    if (write(user.getFileDescriptor(), "break", 6) < 0)throw "[server] Error: write() to client.\n";
}

void MenuO::getConversations(User &user) {

    database->getConversationsList(user);

    if (write(user.getFileDescriptor(), "break", 6) < 0) throw "[server] Error: write() to client.\n";
}
