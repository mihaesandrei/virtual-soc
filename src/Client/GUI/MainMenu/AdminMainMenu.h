//
// Created by andrei on 12.01.2016.
//

#ifndef VIRTUALSOC_ADMINMAINMENU_H
#define VIRTUALSOC_ADMINMAINMENU_H



#include <QWidget>
#include <QFrame>
#include <QGridLayout>
#include <QLabel>
#include <QScrollArea>
#include <qtablewidget.h>

class AdminMainMenu : public QWidget {

Q_OBJECT

public:
    AdminMainMenu(QWidget *parent = 0, int *socketDescriptor = 0);
    QFrame *getFrame();

    void setVisibility(bool visible);

private:
    QFrame *frame;
    QGridLayout *grid;

    int *socketDescriptor;

    QTableWidget *table;
};


#endif //VIRTUALSOC_ADMINMAINMENU_H
