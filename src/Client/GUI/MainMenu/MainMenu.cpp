#include "MainMenu.h"

MainMenu::MainMenu(QWidget *parent) {

    frame = new QFrame(this);

    grid = new QGridLayout(this);

    frame->setLayout(grid);
}

void MainMenu::setVisibility(bool visible) {
    welcomeMessage->setVisible(visible);
    addFriendButton->setVisible(visible);
    viewFriendListButton->setVisible(visible);
    privacySettingsButton->setVisible(visible);
    makePostButton->setVisible(visible);
    viewRecentPostsButton->setVisible(visible);
    chatRoom->setVisible(visible);
    logOut->setVisible(visible);
}

void MainMenu::removeMainMenu() {
    grid->removeWidget(welcomeMessage);
    grid->removeWidget(addFriendButton);
    grid->removeWidget(viewFriendListButton);
    grid->removeWidget(privacySettingsButton);
    grid->removeWidget(makePostButton);
    grid->removeWidget(viewRecentPostsButton);
    grid->removeWidget(chatRoom);
    setVisibility(false);
}

QFrame *MainMenu::getFrame() {
    addFriendButton = new QPushButton("Add Friend", frame);
    connect(addFriendButton, SIGNAL(clicked()), this, SLOT(addFriendListener()));

    viewFriendListButton = new QPushButton("View Friend List", frame);
    connect(viewFriendListButton, SIGNAL(clicked()), this, SLOT(viewFriendListListener()));

    privacySettingsButton = new QPushButton("Privacy Settings", this);
    connect(privacySettingsButton, SIGNAL(clicked()), this, SLOT(privacySettingsListener()));

    makePostButton = new QPushButton("Make Post", this);
    connect(makePostButton, SIGNAL(clicked()), this, SLOT(makePostListener()));

    viewRecentPostsButton = new QPushButton("View Recent Posts", this);
    connect(viewRecentPostsButton, SIGNAL(clicked()), this, SLOT(viewRecentPostsListener()));

    chatRoom = new QPushButton("Chat Room", this);
    connect(chatRoom, SIGNAL(clicked()), this, SLOT(chatRoomListener()));

    grid->addWidget(welcomeMessage, 0, 0, Qt::AlignCenter);
    grid->addWidget(addFriendButton, 1, 0);
    grid->addWidget(viewFriendListButton, 2, 0);
    grid->addWidget(privacySettingsButton, 3, 0);
    grid->addWidget(makePostButton, 4, 0);
    grid->addWidget(viewRecentPostsButton, 5, 0);
    grid->addWidget(chatRoom, 6, 0);

    return this->frame;
}

void MainMenu::setSocketDescriptor(int *fd) {
    this->socketDescriptor = fd;
}

int MainMenu::getSocketDescriptor() {
    return *this->socketDescriptor;
}

void MainMenu::addMainMenu() {

    addFriendButton = new QPushButton("Add Friend", frame);
    connect(addFriendButton, SIGNAL(clicked()), this, SLOT(addFriendListener()));

    viewFriendListButton = new QPushButton("View Friend List", frame);
    connect(viewFriendListButton, SIGNAL(clicked()), this, SLOT(viewFriendListListener()));

    privacySettingsButton = new QPushButton("Privacy Settings", this);
    connect(privacySettingsButton, SIGNAL(clicked()), this, SLOT(privacySettingsListener()));

    makePostButton = new QPushButton("Make Post", this);
    connect(makePostButton, SIGNAL(clicked()), this, SLOT(makePostListener()));

    viewRecentPostsButton = new QPushButton("View Recent Posts", this);
    connect(viewRecentPostsButton, SIGNAL(clicked()), this, SLOT(viewRecentPostsListener()));

    chatRoom = new QPushButton("Chat Room", this);
    connect(chatRoom, SIGNAL(clicked()), this, SLOT(chatRoomListener()));

    grid->addWidget(welcomeMessage, 0, 0, Qt::AlignCenter);
    grid->addWidget(addFriendButton, 1, 0);
    grid->addWidget(viewFriendListButton, 2, 0);
    grid->addWidget(privacySettingsButton, 3, 0);
    grid->addWidget(makePostButton, 4, 0);
    grid->addWidget(viewRecentPostsButton, 5, 0);
    grid->addWidget(chatRoom, 6, 0);
    setVisibility(true);
}

void MainMenu::addFriendListener() {
    removeMainMenu();

    addFriendFrame = new AddFriendFrame();
    addFriendFrame->setSocketDescriptor(socketDescriptor);

    backFromAddFriendButton = new QPushButton("Back", this);
    connect(backFromAddFriendButton, SIGNAL(clicked()), this, SLOT(backFromAddFriend()));

    grid->addWidget(backFromAddFriendButton, 0, 0);
    grid->addWidget(addFriendFrame, 1, 0);
}

void MainMenu::backFromAddFriend() {
    addFriendFrame->getFrame()->setVisible(false);
    delete backFromAddFriendButton;
    this->addMainMenu();
}

void MainMenu::viewFriendListListener() {
    removeMainMenu();

    viewFriendListFrame = new ViewFriendListFrame();
    viewFriendListFrame->setSocketDescriptor(socketDescriptor);

    backFromViewFriendListButton = new QPushButton("Back", this);
    connect(backFromViewFriendListButton, SIGNAL(clicked()), this, SLOT(backFromViewFriendList()));

    grid->addWidget(backFromViewFriendListButton, 0, 0);
    grid->addWidget(viewFriendListFrame->getFrame(), 1, 0);
}

void MainMenu::backFromViewFriendList() {
    delete backFromViewFriendListButton;
    viewFriendListFrame->setVisibility(false);
    this->addMainMenu();
}

void MainMenu::privacySettingsListener() {
    removeMainMenu();
    privacySettingsFrame = new PrivacySettingsFrame();
    privacySettingsFrame->setSocketDescriptor(socketDescriptor);

    backFromPrivacySettingsButton = new QPushButton("Back", this);
    connect(backFromPrivacySettingsButton, SIGNAL(clicked()), this, SLOT(backFromPrivacySettings()));

    grid->addWidget(backFromPrivacySettingsButton, 0, 0);
    grid->addWidget(privacySettingsFrame->getFrame(), 1, 0);
}

void MainMenu::backFromPrivacySettings() {
    delete backFromPrivacySettingsButton;
    privacySettingsFrame->setVisibility(false);
    this->addMainMenu();
}

void MainMenu::makePostListener() {
    removeMainMenu();

    backFromMakePostButton = new QPushButton("Back", this);
    connect(backFromMakePostButton, SIGNAL(clicked()), this, SLOT(backFromMakePost()));

    makePostFrame = new MakePostFrame();
    makePostFrame->setSocketDescriptor(socketDescriptor);

    grid->addWidget(backFromMakePostButton, 0, 0);
    grid->addWidget(makePostFrame->getFrame(), 1, 0);
}

void MainMenu::backFromMakePost() {
    delete backFromMakePostButton;
    makePostFrame->setVisibility(false);
    this->addMainMenu();
}

void MainMenu::viewRecentPostsListener() {
    removeMainMenu();

    backFromRecentPostsButton = new QPushButton("Back", this);
    connect(backFromRecentPostsButton, SIGNAL(clicked()), this, SLOT(backFromRecentPosts()));

    recentPostsFrame = new RecentPostsFrame();
    recentPostsFrame->setSocketDescriptor(socketDescriptor);

    grid->addWidget(backFromRecentPostsButton, 0, 0);
    grid->addWidget(recentPostsFrame->getFrame(), 1, 0);
}

void MainMenu::backFromRecentPosts() {
    delete backFromRecentPostsButton;
    recentPostsFrame->setVisibility(false);
    this->addMainMenu();

}

void MainMenu::chatRoomListener() {
    removeMainMenu();

    backFromChatRoomButton = new QPushButton("Back", this);
    connect(backFromChatRoomButton, SIGNAL(clicked()), this, SLOT(backFromChatRoom()));

    chatRoomFrame = new ChatRoomFrame();
    chatRoomFrame->setSocketDescriptor(socketDescriptor);
    chatRoomFrame->backFromChatRoomButton = backFromChatRoomButton;

    grid->addWidget(backFromChatRoomButton, 0, 0, Qt::AlignTop);
    grid->addWidget(chatRoomFrame->getFrame(), 1, 0);
}

void MainMenu::backFromChatRoom() {
    delete backFromChatRoomButton;
    chatRoomFrame->setVisibility(false);
    this->addMainMenu();
}

void MainMenu::setWelcomeMessage(char *email) {
    email = Utility::concatenate((char *) "Welcome back, ", email);
    email = Utility::concatenate(email, (char *) "!");
    welcomeMessage = new QLabel(email, this);

    QFont font( "Arial", 13);
    welcomeMessage->setFont(font);
}
