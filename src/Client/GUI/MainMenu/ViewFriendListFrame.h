#ifndef VIRTUALSOC_VIEWFRIENDLISTFRAME_H
#define VIRTUALSOC_VIEWFRIENDLISTFRAME_H


#include <QWidget>
#include <QFrame>
#include <QGridLayout>
#include <QLabel>
#include <QScrollArea>
#include <qtablewidget.h>

#include "src/Client/Options.h"

class ViewFriendListFrame : public QWidget{

Q_OBJECT

public:
    ViewFriendListFrame(QWidget *parent = 0);
    QFrame* getFrame();

    void setVisibility(bool visible);

    void setSocketDescriptor(int *fd);
    int getSocketDescriptor();

private:
    QFrame *frame;
    QGridLayout *grid;

    int *socketDescriptor;

    QTableWidget *table;
};


#endif //VIRTUALSOC_VIEWFRIENDLISTFRAME_H
