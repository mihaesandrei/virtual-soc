#include "RecentPostsFrame.h"

RecentPostsFrame::RecentPostsFrame(QWidget *parent) {

    frame = new QFrame(this);

    grid = new QGridLayout(this);

    frame->setLayout(grid);
}

QFrame *RecentPostsFrame::getFrame() {
    char* text = Options::viewRecentPosts(*socketDescriptor);
    recentPosts = new QLabel(text, frame);
    QFont font( "Arial", 12);
    recentPosts->setFont(font);


    QLabel *title = new QLabel("Recent posts:", frame);
    title->setTextFormat(Qt::TextFormat::RichText);


    scrollArea = new QScrollArea;
    scrollArea->setBackgroundRole(QPalette::Light);
    scrollArea->setWidget(recentPosts);

    grid->addWidget(title, 0, 0);
    grid->addWidget(scrollArea, 1, 0);
    return frame;
}

void RecentPostsFrame::setSocketDescriptor(int *fd) {
    socketDescriptor = fd;
}

int RecentPostsFrame::getSocketDescriptor() {
    return *socketDescriptor;
}

void RecentPostsFrame::setVisibility(bool visible) {
    frame->setVisible(visible);
}
