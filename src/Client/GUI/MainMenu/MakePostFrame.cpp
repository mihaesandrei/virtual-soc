#include <qmessagebox.h>
#include "MakePostFrame.h"

MakePostFrame::MakePostFrame(QWidget *parent) {

    frame = new QFrame(this);

    privacyLabel = new QLabel("Privacy: ", this);
    publicPrivacy = new QCheckBox("Public", this);
    connect(publicPrivacy, SIGNAL(stateChanged(int)), this, SLOT(checkPublic(int)));
    familyPrivacy = new QCheckBox("Family", this);
    connect(familyPrivacy, SIGNAL(stateChanged(int)), this, SLOT(checkFamily(int)));
    privatePrivacy = new QCheckBox("Private", this);
    connect(privatePrivacy, SIGNAL(stateChanged(int)), this, SLOT(checkPrivate(int)));

    postLabel = new QLabel("Post: ", this);
    postText = new QTextEdit(this);

    QFrame *privacyFrame = new QFrame(this);
    QGridLayout *privacyGrid = new QGridLayout(privacyFrame);

    privacyGrid->addWidget(privacyLabel, 0, 0);
    privacyGrid->addWidget(publicPrivacy, 0, 1);
    privacyGrid->addWidget(familyPrivacy, 0, 2);
    privacyGrid->addWidget(privatePrivacy, 0, 3);
    privacyFrame->setLayout(privacyGrid);

    postButton = new QPushButton("Post", this);
    connect(postButton, SIGNAL(clicked()), this, SLOT(postListener()));

    grid = new QGridLayout(this);

    grid->addWidget(privacyFrame);
    grid->addWidget(postLabel);
    grid->addWidget(postText);
    grid->addWidget(postButton);

    frame->setLayout(grid);
}

QFrame *MakePostFrame::getFrame() {
    return frame;
}

void MakePostFrame::setSocketDescriptor(int *fd) {
    socketDescriptor = fd;
}

int MakePostFrame::getSocketDescriptor() {
    return *this->socketDescriptor;
}

void MakePostFrame::setVisibility(bool visibile) {
    frame->setVisible(visibile);
}

void MakePostFrame::checkPublic(int state) {
    if(state == Qt::Checked) {
        familyPrivacy->setCheckState(Qt::Unchecked);
        privatePrivacy->setCheckState(Qt::Unchecked);
        privacy = (char *) "Public";
    }
}

void MakePostFrame::checkFamily(int state) {
    if(state == Qt::Checked) {
        publicPrivacy->setCheckState(Qt::Unchecked);
        privatePrivacy->setCheckState(Qt::Unchecked);
        privacy = (char *) "Family";
    }
}

void MakePostFrame::checkPrivate(int state) {
    if(state == Qt::Checked) {
        publicPrivacy->setCheckState(Qt::Unchecked);
        familyPrivacy->setCheckState(Qt::Unchecked);
        privacy = (char *) "Private";
    }
}

void MakePostFrame::postListener() {
    if (strcmp(postText->toPlainText().toLatin1().data(), "") == 0)
        QMessageBox::warning(NULL, "Message", "You didn't write anything yet.\n");
    else if (publicPrivacy->checkState() == Qt::Unchecked && familyPrivacy->checkState() == Qt::Unchecked &&
        privatePrivacy->checkState() == Qt::Unchecked) {

        QMessageBox::warning(NULL, "Message", "You need to choose privacy's post.\n");
    } else {
        char *text = (char *) "";
        text = Utility::concatenate(text, postText->toPlainText().toLatin1().data());
        Options::makePost(text, privacy, *socketDescriptor);
        QMessageBox::information(NULL, "Message", "Post submited!\n");
    }
}
