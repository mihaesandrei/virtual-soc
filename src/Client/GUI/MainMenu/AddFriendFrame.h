#ifndef VIRTUALSOC_ADDFRIENDFRAME_H
#define VIRTUALSOC_ADDFRIENDFRAME_H

#include <QWidget>
#include <QFrame>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QCheckBox>

#include "src/Client/Options.h"

class AddFriendFrame : public QWidget {

Q_OBJECT

public:
    AddFriendFrame(QWidget *parent = 0);
    QFrame* getFrame();

    void setSocketDescriptor(int *fd);
    int getSocketDescriptor();

private slots:
    void addFriendListener();
    void checkFriend(int state);
    void checkFamily(int state);

private:
    QFrame *frame;
    QGridLayout *grid;

    int* socketDescriptor;

    QLabel *emailLabel;
    QLabel *typeLabel;
    QLineEdit  *emailFriend;
    QPushButton *addFriendButton;

    QCheckBox *friendType;
    QCheckBox *familyType;
    char* type;
};

#endif//VIRTUALSOC_ADDFRIENDFRAME_H
