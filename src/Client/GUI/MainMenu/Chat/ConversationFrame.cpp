#include "ConversationFrame.h"

ConversationFrame::ConversationFrame(QWidget *parent, int *fd) {
    socketDescriptor = fd;
    setMouseTracking(true);
    frame = new QFrame(this);
    grid = new QGridLayout(this);

    listWidget = new QListWidget(this);

    message = new QTextEdit(this);

    frame->setLayout(grid);
}

QFrame *ConversationFrame::getFrame() {
    send = new QPushButton("Send", this);
    connect(send, SIGNAL(clicked()), this, SLOT(sendListener()));

    conversationNameLabel = new QLabel(conversationName, this);
    grid->addWidget(conversationNameLabel, 0, 0);
    grid->addWidget(new QLabel("Participants"), 0, 3, Qt::AlignmentFlag::AlignLeft);
    grid->addWidget(listWidget, 1, 3, Qt::AlignmentFlag::AlignRight);

    addParticipants();


    messagesFrame = new MessagesFrame(this, socketDescriptor);
    messagesFrame->conversationName = conversationName;

    grid->addWidget(messagesFrame, 1, 0);
    grid->addWidget(message);
    grid->addWidget(send, 2, 3);
    return frame;
}

void ConversationFrame::addParticipants() const {
    vector<string> participantsEmail = Options::getParticipants(conversationName, *socketDescriptor);
    for (int i = 0; i < participantsEmail.size(); i++) {
        new QListWidgetItem(participantsEmail.at(i).data(), listWidget);
    }
}

void ConversationFrame::setVisibility(bool visible) {
    frame->setVisible(visible);
}

void ConversationFrame::setConversationName(char* c) {
    conversationName = (char *) malloc(sizeof(c));
    strcpy(conversationName, c);
}

void ConversationFrame::sendListener() {
    char *messageToSend = message->toPlainText().toLatin1().data();

    Options::sendMessage(conversationName, messageToSend, *socketDescriptor);
    message->setText("");

    pthread_t thread;
    if (pthread_create(&thread, NULL, messagesFrame->update, NULL) != 0)
        throw "[server] Error: failed to create thread.\n";
    pthread_detach(pthread_self());

}

