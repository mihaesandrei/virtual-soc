#include "ConnectMenu.h"

ConnectMenu::ConnectMenu(QWidget *parent) {

    frame = new QFrame(this);

    ipServer = new QLabel("Server IP:", frame);
    portServer = new QLabel("Server Port:", frame);

    ip = new QLineEdit("127.0.0.1",frame);
    port = new QLineEdit("8321",frame);

    grid = new QGridLayout(frame);
    grid->addWidget(ipServer, 1, 0, Qt::AlignCenter);
    grid->addWidget(ip, 1, 1, Qt::AlignCenter);
    grid->addWidget(portServer, 2, 0, Qt::AlignCenter);
    grid->addWidget(port, 2, 1, Qt::AlignCenter);

    frame->setLayout(grid);
}

QFrame *ConnectMenu::getFrame() {
    return this->frame;
}

ConnectMenu::~ConnectMenu() {
    delete this->ip;
    delete this->port;
    delete this->ipServer;
    delete this->portServer;
    delete this->grid;
    delete this->frame;
}

char *ConnectMenu::getIP() {
    return this->ip->text().toLatin1().data();
}

char *ConnectMenu::getPort() {
    return this->port->text().toLatin1().data();
}
