#include "SignUpFrame.h"

SignUpFrame::SignUpFrame(QWidget *parent) {
    frame = new QFrame(this);

    firstNameLabel = new QLabel("First name: ", frame);
    surnameLabel = new QLabel("Surname: ", frame);
    emailLabel = new QLabel("E-mail: ", frame);
    passwordLabel = new QLabel("Password: ", frame);

    firstName = new QLineEdit("", frame);
    surname = new QLineEdit("", frame);
    email = new QLineEdit("", frame);
    password = new QLineEdit("", frame);
    password->setEchoMode(QLineEdit::Password);

    signUpButton = new QPushButton("Sign Up", frame);
    connect(signUpButton, SIGNAL(clicked()), this, SLOT(signUpListener()));

    grid = new QGridLayout(frame);
    grid->addWidget(firstNameLabel, 1, 0);
    grid->addWidget(firstName, 1, 1);
    grid->addWidget(surnameLabel, 2, 0);
    grid->addWidget(surname, 2, 1);
    grid->addWidget(emailLabel, 3, 0);
    grid->addWidget(email, 3, 1);
    grid->addWidget(passwordLabel, 4, 0);
    grid->addWidget(password, 4, 1);
    grid->addWidget(signUpButton, 5, 1);

    asAdministrator = new QCheckBox("As Administrator", this);
    connect(asAdministrator, SIGNAL(stateChanged(int)), this, SLOT(checkAdminState(int)));

    grid->addWidget(asAdministrator, 5, 0);

    frame->setLayout(grid);
}

QFrame *SignUpFrame::getFrame() {
    return this->frame;
}

void SignUpFrame::signUpListener() {
    if(strcmp(this->firstName->text().toLatin1().data(), "") == 0 )
        QMessageBox::warning(NULL, "Message", "You didn't insert firstname!");
    else if(strcmp(this->surname->text().toLatin1().data(), "") == 0)
        QMessageBox::warning(NULL, "Message", "You didn't insert surname!");
    else if(strcmp(this->email->text().toLatin1().data(), "") == 0)
        QMessageBox::warning(NULL, "Message", "You didn't insert email!");
    else if(strcmp(this->password->text().toLatin1().data(), "") == 0 )
        QMessageBox::warning(NULL, "Message", "You didn't insert password!");
    else {

        if(checked) {
            signUpAsAdministrator();
        }
        else {
            signUpAsUser();
        }
    }
}

void SignUpFrame::signUpAsUser() {
    bool inserted = Options::signUp(firstName->text().toLatin1().data(),
                                    surname->text().toLatin1().data(),

                                    email->text().toLatin1().data(),
                                    password->text().toLatin1().data(),
                                    this->getSocketDescriptor());

    if (inserted) {
                QMessageBox::information(NULL, "Message", "You have been successfully registered!");
                setBlankFields();
            }
            else {
                QMessageBox::warning(NULL, "Message", "The e-mail address you specified is already in use.");

            }
}

void SignUpFrame::signUpAsAdministrator() {
    bool inserted = Options::signUpAsAdmin(firstName->text().toLatin1().data(),
                                    surname->text().toLatin1().data(),

                                    email->text().toLatin1().data(),
                                    password->text().toLatin1().data(),
                                    this->getSocketDescriptor());

    if (inserted) {
        QMessageBox::information(NULL, "Message", "You have been successfully registered as Administrator!");
        setBlankFields();
    }
    else {
        QMessageBox::warning(NULL, "Message", "The e-mail address you specified is not a valid one.");

    }
}

void SignUpFrame::setBlankFields() const {
    firstName->setText("");
    surname->setText("");
    email->setText("");
    password->setText("");
}

void SignUpFrame::setSocketDescriptor(int *fd) {
    this->socketDescriptor = fd;
}

int SignUpFrame::getSocketDescriptor() {
    return *this->socketDescriptor;
}

void SignUpFrame::checkAdminState(int state) {
    checked = state == Qt::Checked;
}
