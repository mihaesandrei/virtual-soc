#ifndef VIRTUALSOC_SIGNUPFRAME_H
#define VIRTUALSOC_SIGNUPFRAME_H

#include <QWidget>
#include <QFrame>
#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>
#include <QPushButton>
#include <QMessageBox>

#include "GuestMenu.h"
#include "src/Client/Options.h"

class GuestMenu;

class SignUpFrame : public QWidget {

Q_OBJECT

public:
    SignUpFrame(QWidget *parent = 0);
    QFrame* getFrame();

    void setSocketDescriptor(int *fd);
    int getSocketDescriptor();

private slots:
    void signUpListener();
    void checkAdminState(int state);
private:
    bool checked = false;

private:
    QFrame *frame;
    QGridLayout *grid;

    int* socketDescriptor;

    QLabel *firstNameLabel;
    QLabel *surnameLabel;
    QLabel *emailLabel;
    QLabel *passwordLabel;

    QLineEdit *firstName;
    QLineEdit *surname;
    QLineEdit *email;
    QLineEdit *password;

    QPushButton *signUpButton;
    QPushButton *backFromSignUp;

    QCheckBox *asAdministrator;

    void setBlankFields() const;

    void signUpAsUser();

    void signUpAsAdministrator();
};

#endif //VIRTUALSOC_SIGNUPFRAME_H
