#ifndef VIRTUALSOC_PUBLICPOSTSFRAME_H
#define VIRTUALSOC_PUBLICPOSTSFRAME_H

#include <QWidget>
#include <QFrame>
#include <QGridLayout>
#include <QLabel>
#include <QScrollArea>

#include "src/Client/Options.h"

class PublicPostsFrame : public QWidget {

Q_OBJECT

public:
    PublicPostsFrame(QWidget *parent = 0);

    QFrame* getFrame();

    void setSocketDescriptor(int* fd);
    int getSocketDescriptor();

    void setVisibility(bool visibile);

private:
    QFrame *frame;
    QGridLayout *grid;

    int *socketDescriptor;

    QScrollArea * scrollArea;
    QLabel *publicPosts;
};

#endif //VIRTUALSOC_PUBLICPOSTSFRAME_H
