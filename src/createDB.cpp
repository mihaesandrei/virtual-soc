#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h> 

static int callback(void *NotUsed, int argc, char **argv, char **azColName){
    for(int i = 0; i<argc; i++)
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    printf("\n");
    return 0;
}

int main() {

    sqlite3 *db;
    char *zErrMsg = 0;
    int rc;
    char *sql;

    if (remove("database.db") == 0) printf("Database deleted successfully!\n");
    else printf("Error: unable to delete the file.\n");

    /* Open database */
    rc = sqlite3_open("database.db", &db);
    if (rc) {  fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db)); exit(0); }
    else fprintf(stdout, "Opened database successfully\n");

    /* Create SQL statement */
    sql = (char *) "CREATE TABLE USERS("  \
     "ID                INTEGER        PRIMARY KEY   AUTOINCREMENT, " \
     "FIRSTNAME         TEXT     		NOT NULL," \
     "SURNAME           TEXT     		NOT NULL," \
     "PASSWORD          TEXT           NOT NULL," \
     "EMAIL             TEXT           NOT NULL UNIQUE,"
     "PROFILE_PRIVACY   TEXT           NOT NULL,"\
     "POSTS_PRIVACY     TEXT           NOT NULL,"\
     "STATUS            TEXT           NOT NULL," \
     "IP                TEXT);"

            "CREATE TABLE FRIENDSHIP("  \
     "FIRST_ID           INTEGER        NOT NULL, " \
     "SECOND_ID          INTEGER        NOT NULL," \
     "TYPE 	            TEXT            NOT NULL," \
     "PRIMARY KEY(FIRST_ID, SECOND_ID), "
     "FOREIGN KEY(FIRST_ID) REFERENCES users(ID)," \
     "FOREIGN KEY(SECOND_ID) REFERENCES users(ID)) ;"

            "CREATE TABLE POSTS("  \
     "USER_ID          INTEGER        NOT NULL, " \
     "POST_DATE        DATE           NOT NULL,  "
     "POST 	           TEXT           NOT NULL," \
     "POST_PRIVACY 	   TEXT           NOT NULL," \
     "FOREIGN KEY(USER_ID) REFERENCES users(ID));"

            "CREATE TABLE CONVERSATION("  \
     "NAME           TEXT            NOT NULL, " \
     "OWNER_EMAIL    TEXT            NOT NULL," \
     "PRIMARY KEY(NAME), "
     "FOREIGN KEY(OWNER_EMAIL) REFERENCES users(EMAIL));"

            "CREATE TABLE PARTICIPANTS(" \
     "CONVERSATION_NAME       TEXT           NOT NULL," \
     "PARTICIPANT_EMAIL       TEXT           NOT NULL," \
     "PRIMARY KEY(CONVERSATION_NAME, PARTICIPANT_EMAIL), "
     "FOREIGN KEY(CONVERSATION_NAME) REFERENCES conversation(NAME),"
     "FOREIGN KEY(PARTICIPANT_EMAIL) REFERENCES users(EMAIL));"

            "CREATE TABLE MESSAGES("  \
     "MESSAGE_ID              INTEGER        PRIMARY KEY   AUTOINCREMENT, " \
     "CONVERSATION_NAME       TEXT           NOT NULL, " \
     "MESSAGE                 TEXT                   , "
     "SENDER_EMAIL 	          TEXT           NOT NULL, " \
     "DATE 	                  DATE           NOT NULL, " \
     "FOREIGN KEY(CONVERSATION_NAME) REFERENCES conversation(NAME),"
     "FOREIGN KEY(SENDER_EMAIL) REFERENCES users(EMAIL));"

            "CREATE TABLE validEmails("  \
     "EMAIL                  TEXT         NOT NULL,"
     "FOREIGN KEY(EMAIL) REFERENCES Administrators(EMAIL));"

     "INSERT INTO validEmails (EMAIL) VALUES('admin'); "

            "CREATE TABLE Administrators("  \
     "ID                INTEGER        PRIMARY KEY   AUTOINCREMENT, " \
     "FIRSTNAME         TEXT     		NOT NULL," \
     "SURNAME           TEXT     		NOT NULL," \
     "PASSWORD          TEXT           NOT NULL," \
     "EMAIL             TEXT           NOT NULL UNIQUE);";


    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

    if (rc != SQLITE_OK) {  fprintf(stderr, "SQL error: %s\n", zErrMsg); sqlite3_free(zErrMsg); }
    else fprintf(stdout, "Table created successfully\n");

    sqlite3_close(db);
    return 0;
}